import { Component } from '@angular/core';
import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular16App';
  layoutBlockVariation = LayoutBlockVariation;
}
