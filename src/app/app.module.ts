import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DigiArbetsformedlingenAngularModule } from '@digi/arbetsformedlingen-angular';


import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DigiArbetsformedlingenAngularModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
